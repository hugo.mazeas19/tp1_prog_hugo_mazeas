package com.dinfogarneau.coursmobile.exercices.tp1_progmob;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    private Button btn_connecter;
    private EditText txtUsername;
    private EditText txtPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authentification);

        btn_connecter = findViewById(R.id.btnConnexion);
        txtPassword = findViewById(R.id.txtPassword);
        txtUsername = findViewById(R.id.txtEmail);

        btn_connecter.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        Intent intent;
        if (txtPassword.getText().toString().equals("123") && txtUsername.getText().toString().equals("bot")){
            intent = new Intent(this, JeuActivity.class);
            startActivity(intent);
        }else{
            Toast.makeText(this, R.string.wrongpassword, Toast.LENGTH_LONG).show();
        }
    }
}