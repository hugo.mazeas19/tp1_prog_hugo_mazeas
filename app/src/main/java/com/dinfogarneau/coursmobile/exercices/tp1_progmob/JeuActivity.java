package com.dinfogarneau.coursmobile.exercices.tp1_progmob;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;


public class JeuActivity extends AppCompatActivity implements View.OnClickListener {


    //Déclaration des attributs
    SharedPreferences prefs;
    private Button btnConnection;
    private Button btnSetting;
    private Button btnGO;
    private Button btn1;
    private Button btn2;
    private Button btn3;
    private Button btn4;
    private TextView lblScore;
    private TextView tvNomJoueur;

    private int REQUEST_CODE = 1234;
    private int ButtonToClick = 0;
    private long TimeStart = 0;
    private long TempsBouton2 = 0;
    private long TempsBouton3 = 0;
    private String Record;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pagejeu);

        prefs = getSharedPreferences("infoApp", MODE_PRIVATE);
        Record = prefs.getString("score", "None");
        String nom = prefs.getString("nom", "bob");

        // Find View By Id

        btnConnection = findViewById(R.id.btnConnexion);
        btnSetting = findViewById(R.id.btnSettings);
        tvNomJoueur = findViewById(R.id.lblNomJoueur);
        btn1 = findViewById(R.id.btn1);
        btn2 = findViewById(R.id.btn2);
        btn3 = findViewById(R.id.btn3);
        btn4 = findViewById(R.id.btn4);
        btnGO = findViewById(R.id.btnGO);
        lblScore = findViewById(R.id.lblHighestScore);

        // =========================================

        if(tvNomJoueur.getText().toString().equals("null")){
            tvNomJoueur.setText("bot");
        }
        if (savedInstanceState == null){
            tvNomJoueur.setText("null");
        }

        // Event Listener
        btnSetting.setOnClickListener(this);
        btnGO.setOnClickListener(this);
        btn1.setOnClickListener(this);
        btn2.setOnClickListener(this);
        btn3.setOnClickListener(this);
        btn4.setOnClickListener(this);
        //=========================================

        lblScore.setText(Record);
        tvNomJoueur.setText(nom);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.btnSettings:
                intent = new Intent(this, SettingActivity.class);
                intent.putExtra("nom", tvNomJoueur.getText());
                startActivityForResult(intent, REQUEST_CODE);
                break;
            case R.id.btnGO:

                //Mise à zéro de la parti

                btn1.setText(R.string.un);
                btn2.setText(R.string.deux);
                btn3.setText(R.string.trois);
                btn4.setText(R.string.quatre);
                btn1.getBackground().setAlpha(255);
                btn2.getBackground().setAlpha(255);
                btn3.getBackground().setAlpha(255);
                btn4.getBackground().setAlpha(255);
                ButtonToClick = 1;
                break;
            case R.id.btn1:
                if (ButtonToClick == 1){
                    TimeStart = System.currentTimeMillis();
                    ButtonToClick++;
                    btn1.setText(R.string.gobutton);
                    btn1.getBackground().setAlpha(100);
                }
                break;
            case R.id.btn2:
                if (ButtonToClick == 2){
                    ButtonToClick++;
                    long temps = System.currentTimeMillis() - TimeStart;
                    TempsBouton2 = temps;
                    btn2.setText(temps + "MS");
                    btn2.getBackground().setAlpha(100);
                }
                break;
            case R.id.btn3:
                if (ButtonToClick == 3){
                    ButtonToClick++;
                    long temps = System.currentTimeMillis() - TimeStart;
                    TempsBouton3 = temps;
                    btn3.setText(temps + "MS");
                    btn3.getBackground().setAlpha(100);
                }
                break;
            case R.id.btn4:
                if (ButtonToClick == 4){
                    ButtonToClick = 0;
                    long TimeEnd = System.currentTimeMillis();
                    long TimeTotal = TimeEnd - TimeStart;
                    btn4.setText(TimeTotal + "MS");
                    btn4.getBackground().setAlpha(100);

                    //Vérification du nouveau pointage

                    if (Record != "None"){
                        if (TimeTotal < Long.valueOf(Record) || Record == "0"){
                            lblScore.setText(TimeTotal + "ms");
                            Record = String.valueOf(TimeTotal);
                            SharedPreferences.Editor editor = prefs.edit();
                            editor.putString("score", Record);
                            editor.apply();
                        }
                    }else{
                        Record = String.valueOf(TimeTotal);
                        lblScore.setText(TimeTotal + "ms");
                        SharedPreferences.Editor editor = prefs.edit();
                        editor.putString("score", Record);
                        editor.apply();
                    }

                    //Si entre 1 et 2 secondes

                    if (TimeTotal > 1000 && TimeTotal < 2000){
                        intent = new Intent(this, VictoireActivity.class);
                        intent.putExtra("score", String.valueOf(TimeTotal));
                        startActivity(intent);
                    }
                }
                break;
        }
    }

    @Override
    protected void onActivityResult(int req, int res, @Nullable Intent data) {
        super.onActivityResult(req, res, data);
        if (req == REQUEST_CODE && res == RESULT_OK) {
            String message = data.getStringExtra(SettingActivity.NOUVNOM);
            if (data.getStringExtra(SettingActivity.RESET).equals("true")){
                lblScore.setText("0");
            }

            tvNomJoueur.setText(message);
        } else {
            tvNomJoueur.setText(R.string.erreur);
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        //Sauvegarde des données
        outState.putString("nom", tvNomJoueur.toString());
        outState.putInt("progression", ButtonToClick);
        outState.putLong("tempsDebut", TimeStart);
        outState.putLong("temps2", TempsBouton2);
        outState.putLong("temps3", TempsBouton3);
        super.onSaveInstanceState(outState);
    }


    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        //Récupération des données
        String nom = savedInstanceState.getString("nom");
        int Progression = savedInstanceState.getInt("progression");
        long temps2 = savedInstanceState.getLong("temps2");
        long temps3 = savedInstanceState.getLong("temps3");
        long tempsDebut = savedInstanceState.getLong("tempsDebut");

        //Restauration de la progression de la parti
        TimeStart = tempsDebut;
        ButtonToClick = Progression;

        //Restauration de l'affichage

        if (ButtonToClick == 4){
            btn3.setText(temps3 + "MS");
            btn3.getBackground().setAlpha(100);
            btn2.setText(temps2 + "MS");
            btn2.getBackground().setAlpha(100);
            btn1.setText(R.string.gobutton);
            btn1.getBackground().setAlpha(100);
        }
        if (ButtonToClick == 3){
            btn2.setText(temps2 + "MS");
            btn2.getBackground().setAlpha(100);
            btn1.setText(R.string.gobutton);
            btn1.getBackground().setAlpha(100);
        }
        if (ButtonToClick == 2){
            btn1.setText(R.string.gobutton);
            btn1.getBackground().setAlpha(100);
        }


        super.onRestoreInstanceState(savedInstanceState);
    }
}
