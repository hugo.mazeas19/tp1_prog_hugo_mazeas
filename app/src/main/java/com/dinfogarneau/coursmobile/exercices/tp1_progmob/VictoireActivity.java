package com.dinfogarneau.coursmobile.exercices.tp1_progmob;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

public class VictoireActivity extends AppCompatActivity {

    private TextView lblScore;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image);

        lblScore = findViewById(R.id.lblScore);

        Intent intent = getIntent();
        lblScore.setText(String.valueOf(intent.getStringExtra("score")) + "MS");

    }
}