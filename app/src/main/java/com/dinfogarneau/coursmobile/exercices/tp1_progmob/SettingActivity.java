package com.dinfogarneau.coursmobile.exercices.tp1_progmob;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SettingActivity extends AppCompatActivity implements View.OnClickListener {

    SharedPreferences prefs;
    public static final String NOUVNOM = "NOUVNOM";
    public static final String RESET = "RESET";
    private Button btnOK;
    private EditText txtNouvNom;
    private Button btnReset;
    private Boolean boolReset = false;
    private Button btnShare;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings);

        prefs = getSharedPreferences("infoApp", MODE_PRIVATE);

        //Find View By ID

        btnOK = findViewById(R.id.btnOkSettings);
        txtNouvNom = findViewById(R.id.txtPseudo);
        btnReset = findViewById(R.id.btnResetScore);
        btnShare = findViewById(R.id.btnShare);

        //========================================


        Intent intent = getIntent();
        String nom = intent.getStringExtra("nom");

        txtNouvNom.setText(nom);

        // Event Listener

        btnOK.setOnClickListener(this);
        btnReset.setOnClickListener(this);
        btnShare.setOnClickListener(this);

        //=================================

    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.btnOkSettings:
                String NouvNom = txtNouvNom.getText().toString();

                Intent intent = new Intent();
                intent.putExtra(NOUVNOM, NouvNom);
                intent.putExtra(RESET, boolReset.toString());
                setResult(RESULT_OK, intent);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("nom", NouvNom);
                editor.apply();

                finish();
                break;
            case R.id.btnResetScore:
                SharedPreferences.Editor editor2 = prefs.edit();
                editor2.putString("score", "None");
                editor2.apply();

                boolReset = true;
                Toast.makeText(this, R.string.scorereset, Toast.LENGTH_LONG).show();
                break;
            case R.id.btnShare:
                String score = prefs.getString("score", "None");

                Intent intentShare = new Intent(Intent.ACTION_SEND);
                intentShare.setType("text/plain");
                intentShare.putExtra(Intent.EXTRA_TEXT, getString(R.string.highestscore) + score + "ms");
                startActivity(Intent.createChooser(intentShare, getString(R.string.sharevia)));
                break;
        }
    }
}